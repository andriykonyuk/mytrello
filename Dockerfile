FROM python:3.6

WORKDIR /
ADD . /
RUN pip install -r requirements.txt

EXPOSE 80
ENV NAME TRELLOBACK

# Run app.py when the container launches
CMD ["gunicorn", "-b", "0.0.0.0:80", "trelloBack.wsgi"]

