from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from rest_api.models import User, Board, Task, Lists, BoardLists, ListsTask


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'email')
        # fields = ('__all__')


class BoardSerializer(ModelSerializer):
    class Meta:
        model = Board
        fields = ('__all__')


class ListsSerializer(ModelSerializer):
    class Meta:
        model = Lists
        fields = ('__all__')


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = ('__all__')

    def create(self, validated_data):
        task = Task.objects.create(**validated_data)
        return task

class BoardListsSerializer(ModelSerializer):
    # board = BoardSerializer()
    lists = ListsSerializer()

    class Meta:
        model = BoardLists
        fields = ('__all__')


class ListsTaskSerializer(ModelSerializer):
    lists = ListsSerializer()
    task = TaskSerializer()

    class Meta:
        model = ListsTask
        fields = ('__all__')


class OnlyListSerializer(ModelSerializer):
    # lists = ListsSerializer()
    task = TaskSerializer()

    class Meta:
        model = ListsTask
        fields = ('__all__')

