from __future__ import absolute_import, unicode_literals
from celery import shared_task
from rest_api.models import Board


@shared_task()
def get_user_dashboard(user_id):
    query = Board.objects.all().filter(user=user_id)
    return query


@shared_task()
def print_hw(message):
    print(message)
    return True


@shared_task()
def print_ahw(message):
    print(message)
    return True


@shared_task()
def get_hw():
    r = range(1, 10)
    for i in r:
        print_hw.delay("This is message " + str(i))


@shared_task()
def get_ahw():
    r = range(1, 10)
    for i in r:
        print_ahw.delay("This is message from another task" + str(i))
