import json
from pprint import pprint

from celery.result import AsyncResult
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from pip._internal import req
from rest_framework import permissions, generics, viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt import authentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import mixins
from rest_api.tasks import get_user_dashboard
from rest_api.models import User, Board, Task, BoardLists, Lists, ListsTask
from rest_api.serializers import UserSerializer, BoardSerializer, ListsSerializer, BoardListsSerializer, TaskSerializer, \
    ListsTaskSerializer, OnlyListSerializer


class UsersView(viewsets.GenericViewSet,
                mixins.DestroyModelMixin,
                mixins.UpdateModelMixin,
                mixins.RetrieveModelMixin,
                mixins.CreateModelMixin):
    # authentication_classes = (authentication.JSONWebTokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class BoardView(mixins.UpdateModelMixin,
                mixins.DestroyModelMixin,
                mixins.CreateModelMixin,
                generics.GenericAPIView):
    # authentication_classes = (SessionAuthentication, BasicAuthentication, authentication.JSONWebTokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = Board.objects.all()
    serializer_class = BoardSerializer

    def get(self, request, *args, **kwargs):
        # if request.user.is_authenticated:
        user_id = request.user.id
        query = Board.objects.all().filter(user=1)
        serialize_data = BoardSerializer(query, many=True)
        return JsonResponse(serialize_data.data, safe=False)
        # else:
        #     return HttpResponse(status=404)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ListsView(mixins.UpdateModelMixin,
                mixins.DestroyModelMixin,
                generics.GenericAPIView):
    # authentication_classes = (SessionAuthentication, BasicAuthentication, authentication.JSONWebTokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = Lists.objects.all()
    serializer_class = ListsSerializer

    def get(self, request, pk, *args, **kwargs):
        # if request.user.is_authenticated:
        query = BoardLists.objects.filter(board=pk)
        serialize_data = BoardListsSerializer(query, many=True)
        res = [item["lists"] for item in serialize_data.data]

        for item in res:
            tasks = ListsTask.objects.filter(lists=item['id'])
            tsd = ListsTaskSerializer(tasks, many=True)
            item['tasks'] = [i['task'] for i in tsd.data]

        return JsonResponse(res, safe=False)

    # else:
    #     return HttpResponse(statuts=202)

    def post(self, request, pk, *args, **kwargs):
        print(request.data, pk)
        try:
            board = Board.objects.get(pk=pk)
        except:
            return Response({"message": "Board with this id not created."}, status=status.HTTP_400_BAD_REQUEST)
        print(board.id)
        serializer_list = ListsSerializer(data=request.data)
        serializer_list.is_valid(raise_exception=True)
        lists = Lists.objects.create(**serializer_list.data)

        data_boardlist = {"board": board,
                          "lists": lists}
        board = BoardLists.objects.create(**data_boardlist)
        bs = BoardListsSerializer(board)
        return Response(bs.data, status=status.HTTP_201_CREATED)

    def put(self, request, pk, *args, **kwargs):
        print(pk, request.data)
        item = Lists.objects.get(pk=pk)
        item.name = request.data['name']
        item.save()
        data = ListsSerializer(item)
        return Response(data.data)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class TaskView(mixins.UpdateModelMixin,
               mixins.DestroyModelMixin,
               mixins.CreateModelMixin,
               generics.GenericAPIView):
    # authentication_classes = (SessionAuthentication, BasicAuthentication, authentication.JSONWebTokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get(self, request, pk, *args, **kwargs):
        # if request.user.is_authenticated:
        query = ListsTask.objects.filter(lists=pk)
        serialize_data = ListsTaskSerializer(query, many=True)
        res = [item["task"] for item in serialize_data.data]
        return JsonResponse(res, safe=False)

    # else:
    #     return HttpResponse(status=202)

    def post(self, request, pk, *args, **kwargs):
        print(request.data, pk)
        try:
            list = Lists.objects.get(pk=pk)
        except:
            return Response({"message": "List with this id not created."}, status=status.HTTP_400_BAD_REQUEST)

        serializer_task = TaskSerializer(data=request.data)
        serializer_task.is_valid(raise_exception=True)

        task = Task.objects.create(**serializer_task.data)

        data_listtask = {"task": task,
                         "lists": list}
        list_task = ListsTask.objects.create(**data_listtask)
        ls = ListsTaskSerializer(list_task)
        return Response(ls.data, status=status.HTTP_201_CREATED)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, pk, *args, **kwargs):
        task = Task.objects.get(pk=pk)
        list_task = ListsTask.objects.get(task=task)

        list_task.delete()
        task.delete()
        return self.destroy(request, *args, **kwargs)


class GetResult(APIView):
    def post(self, request, format=None):
        data = request.data
        if "task_id" in data:
            task = AsyncResult(data["task_id"])
            if task.ready():
                result = task.get(timeout=1, propagate=False)
                return JsonResponse(result)
            else:
                return HttpResponse(status=204)
