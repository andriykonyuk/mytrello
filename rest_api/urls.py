import rest_framework
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import refresh_jwt_token

from rest_api.views import BoardView, TaskView
from rest_api.views import UsersView, BoardView, ListsView, GetResult

urlpatterns = [
    url(r'^users/(?P<pk>\d+)/$', UsersView.as_view({'post': 'post'})),
    url(r'^boards/(?P<pk>\d+)', BoardView.as_view()),
    url(r'^lists/(?P<pk>\d+)', ListsView.as_view()),
    url(r'^task/(?P<pk>\d+)', TaskView.as_view()),

    # url(r'^getresult/', GetResult.as_view()),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^refresh-token/', refresh_jwt_token),
]
