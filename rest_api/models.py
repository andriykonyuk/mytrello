from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin


class User(AbstractUser):
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)


class Task(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)



class Lists(models.Model):
    name = models.CharField(max_length=127, unique=True)


class Board(models.Model):
    name = models.CharField(max_length=127)
    background = models.ImageField(upload_to='boards_bg/', null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class BoardLists(models.Model):
    board = models.ForeignKey(Board, unique=False, on_delete=models.CASCADE)
    lists = models.ForeignKey(Lists, unique=False, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('board', 'lists')


class ListsTask(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    lists = models.ForeignKey(Lists, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('task', 'lists')


class TaskUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
