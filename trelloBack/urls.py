from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include("django.contrib.auth.urls")),
    path(r'login', obtain_jwt_token),
    path(r'refresh', refresh_jwt_token),
    url(r"^api/", include('rest_api.urls'))
]
