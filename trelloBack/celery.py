from __future__ import absolute_import, unicode_literals
import os
import random

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'trelloBack.settings.development')
app = Celery('trelloBack')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
app.conf.task_routes = ([
    ('rest_api.tasks.get_hw', {'queue': 'hw'}),
    ('rest_api.tasks.get_ahw', {'queue': 'ahw'}),
],)
app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'rest_api.tasks.get_hw',
        'schedule': 10.0,
    },
    'add-every-5-seconds': {
        'task': 'rest_api.tasks.get_ahw',
        'schedule': 5.0,
    },
}
app.conf.timezone = 'UTC'
#  celery multi start 2 -l INFO -Q:1 hw -Q:2 ahw  DEBUG
