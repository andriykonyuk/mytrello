#!/bin/bash

# Prepare log files and start outputting logs to stdout
mkdir -p ./log
touch ./log/gunicorn.log
touch ./log/gunicorn-access.log
tail -n 0 -f ./log/gunicorn*.log &

export DJANGO_SETTINGS_MODULE=trelloBack.settings.development

exec gunicorn trelloBack.wsgi:application \
    --bind 0.0.0.0:80 \
#exec gunicorn iconx.wsgi:application \
#    --name iconx_django \
#    --bind 0.0.0.0:8000 \
#    --workers 5 \
#    --log-config=./gunicorn.conf
"$@"